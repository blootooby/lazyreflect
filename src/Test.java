import java.lang.reflect.Modifier;

import nf.co.haxter.lazyreflect.wrapper.ClassWrapper;
import nf.co.haxter.lazyreflect.wrapper.FieldWrapper;
import sun.misc.Unsafe;


public final class Test
{
	public static void main(String[] args)
	{
		try
		{
			ClassWrapper unsafeWrapper = new ClassWrapper(sun.misc.Unsafe.class);
			
			FieldWrapper instanceField = unsafeWrapper.guessAndGetFieldWrapper(sun.misc.Unsafe.class);
			instanceField.setModifiers(Modifier.PUBLIC, Modifier.STATIC, Modifier.FINAL);
			Unsafe theUnsafe = (Unsafe) instanceField.getField().get(null);
			System.out.println(theUnsafe.allocateMemory(0x10000989L));
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}

class Test2
{
	private final int field1; // if we declare its value here, then reflection wouldn't change a thing as the references to the class would all get replaced by the value created here.

	public int field2 = 1;

	@SuppressWarnings("unused")
	private int field3 = 2;

	public Test2()
	{
		field1 = 0;
	}

	@SuppressWarnings("unused")
	private static void update(String lol, int String)
	{
		System.out.println(lol);
		System.out.println(String);
	}

	public int getField1()
	{
		return field1;
	}
}