package nf.co.haxter.lazyreflect.wrapper;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.zip.ZipFile;

public class ClassWrapper
{
	private Class<?> theClass;

	public ClassWrapper(Class<?> clazz)
	{
		this.theClass = clazz;
	}

	public Class<?> getTheClass()
	{
		return this.theClass;
	}

	public FieldWrapper getFieldWrapper(String fieldName)
			throws NoSuchFieldException, SecurityException
	{
		return MemberWrapper.getFieldWrapperFromClass(theClass, fieldName);
	}

	public FieldWrapper guessAndGetFieldWrapper(Class<?> fieldType,
			int... modifiers)
	{
		boolean checkFieldType = fieldType != null;

		boolean checkModifiers = modifiers.length != 0;

		if (!checkFieldType && !checkModifiers)
			return new FieldWrapper(this.theClass.getDeclaredFields()[0]);
		int fullMods = 0;
		if (checkModifiers)
			for (int i : modifiers)
				fullMods |= i;
		Field[] f = this.theClass.getDeclaredFields();
		for (Field f1 : f)
		{
			if (checkModifiers)
			{
				if(!checkFieldType)
				{
					if(f1.getModifiers() == fullMods)
						return new FieldWrapper(f1);
				}
				else if (f1.getType().equals(fieldType))
					if (f1.getModifiers() == fullMods)
						return new FieldWrapper(f1);
			} else
			{
				if (f1.getType().equals(fieldType))
					return new FieldWrapper(f1);
			}
		}
		return null;
	}

	public MethodWrapper guessAndGetMethodWrapper(Class<?> returnType,
			Class<?>[] paramTypes, int... modifiers)
	{
		if (returnType == null)
			returnType = Void.TYPE;
		boolean checkModifiers = modifiers.length != 0;
		int fullMods = 0;
		if (checkModifiers)
			for (int i : modifiers)
				fullMods |= i;
		Method[] m = this.theClass.getDeclaredMethods();
		for (Method m1 : m)
		{
			if (checkModifiers)
			{
				if ((m1.getReturnType() == null && returnType == null)
						|| (m1.getReturnType() != null && m1.getReturnType()
								.equals(returnType)))
				{
					if (m1.getModifiers() != fullMods)
						continue;
					Class<?>[] params = m1.getParameterTypes();
					if (params.length != paramTypes.length)
						continue;
					boolean contin = false;
					for (int i = 0; i < params.length; i++)
					{
						if (params[i] == null
								|| !params[i].equals(paramTypes[i]))
						{
							contin = true;
							break;
						}
					}
					if (contin)
						continue;

					return new MethodWrapper(m1);
				}
			} else
			{
				if ((m1.getReturnType() == null && returnType == null)
						|| (m1.getReturnType() != null && m1.getReturnType()
								.equals(returnType)))
				{
					// return type is right.
					Class<?>[] params = m1.getParameterTypes();
					if (params.length != paramTypes.length)
						continue;
					boolean contin = false;
					for (int i = 0; i < params.length; i++)
					{
						if (params[i] == null
								|| !params[i].equals(paramTypes[i]))
						{
							contin = true;
							break;
						}
					}
					if (contin)
						continue;

					return new MethodWrapper(m1);
				}
			}
		}
		return null;
	}

	public MethodWrapper getMethodWrapper(String methodName,
			Class<?>... methodParameterTypes) throws NoSuchMethodException,
			SecurityException
	{
		return MemberWrapper.getMethodWrapperFromClass(theClass, methodName,
				methodParameterTypes);
	}

	public ConstructorWrapper<?> getConstructorWrapper(
			Class<?>... constructorParameterTypes)
			throws NoSuchMethodException, SecurityException
	{
		return MemberWrapper.getConstructorWrapperFromClass(theClass,
				constructorParameterTypes);
	}

	/**
	 * @param file
	 *            The archive file to load the class from. (i.e. *.zip, *.jar)
	 * @param className
	 *            The fully qualified class name of the class that is to be
	 *            loaded from the zip/jar.
	 * @return A ClassWrapper object the encapsulates the class.
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public static ClassWrapper loadClassWrapperFromArchive(File file,
			String className) throws ClassNotFoundException, IOException
	{
		if (file == null)
			return null;
		return loadClassWrapperViaURLArray(new URL[] { file.toURI().toURL() },
				className);
	}

	/**
	 * @param file
	 *            The archive file to load the class from. (i.e. *.zip, *.jar)
	 * @param className
	 *            The fully qualified class name of the class that is to be
	 *            loaded from the zip/jar.
	 * @return A ClassWrapper object the encapsulates the class.
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public static ClassWrapper loadClassWrapperFromArchive(ZipFile file,
			String className) throws ClassNotFoundException, IOException
	{
		if (file == null)
			return null;
		return loadClassWrapperViaURLArray(
				new URL[] { new URL(file.getName()) }, className);
	}

	private static ClassWrapper loadClassWrapperViaURLArray(URL[] urls,
			String className) throws ClassNotFoundException, IOException
	{
		if (urls == null || urls.length < 1)
			return null;
		URLClassLoader cl = new URLClassLoader(urls);
		ClassWrapper b = new ClassWrapper(cl.loadClass(className));
		cl.close();
		return b;
	}

	public static ClassWrapper loadClassWrapperViaBytecode(String name,
			byte[] bytecode) throws NoSuchMethodException, SecurityException,
			IllegalAccessException, IllegalArgumentException,
			InvocationTargetException
	{
		/*
		 * Ooh, we're gonna use some reflection to do some reflection. Quite a
		 * hackish solution, but by doing this, we don't need to use a separate
		 * ClassLoader.
		 */
		ClassWrapper cw = new ClassWrapper(ClassLoader.class);
		MethodWrapper defineClassMethod = cw.getMethodWrapper("defineClass",
				String.class, byte[].class, int.class, int.class);

		defineClassMethod.setAccessible(true);
		Class<?> loadedClass = (Class<?>) defineClassMethod.getMethod().invoke(
				ClassWrapper.class.getClassLoader(), name, bytecode, 0,
				bytecode.length);

		return new ClassWrapper(loadedClass);
	}

	/**
	 * Gets the location on the disk where the class was stored. Some classes
	 * aren't stored exactly on the disk, so those return null. Not really an
	 * exact science.
	 * 
	 * @return The location.
	 * @throws URISyntaxException
	 */
	public String getDiskLocation() throws URISyntaxException
	{
		if (this.theClass.getClassLoader() == null)
			return null;
		return this.theClass.getProtectionDomain().getCodeSource()
				.getLocation().getPath();
	}

}