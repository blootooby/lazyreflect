package nf.co.haxter.lazyreflect.wrapper;

import java.lang.reflect.Constructor;

public class ConstructorWrapper<T> extends MemberWrapper
{
	public ConstructorWrapper(Constructor<T> c)
	{
		super(c);
	}

	@SuppressWarnings("unchecked")
	public Constructor<T> getConstructor()
	{
		return (Constructor<T>)this.theMember;
	}
}
