package nf.co.haxter.lazyreflect.wrapper;

import java.lang.reflect.Field;

public class FieldWrapper extends MemberWrapper
{
	public FieldWrapper(Field f)
	{
		super(f);
	}

	public Field getField()
	{
		return (Field) this.theMember;
	}
}
