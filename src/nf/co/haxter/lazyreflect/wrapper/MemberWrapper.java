package nf.co.haxter.lazyreflect.wrapper;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;

public abstract class MemberWrapper
{
	protected Member theMember;
	protected boolean isAccessibleObject;

	protected static Field methodModifiersField = null;
	protected static Field fieldModifiersField = null;
	protected static Field constructorModifiersField = null;

	static
	{
		try
		{
			methodModifiersField = Method.class.getDeclaredField("modifiers");
			fieldModifiersField = Field.class.getDeclaredField("modifiers");
			constructorModifiersField = Constructor.class
					.getDeclaredField("modifiers");
		} catch (Exception e)
		{
		}
	}

	public MemberWrapper(Member m)
	{
		this.theMember = m;
		this.isAccessibleObject = m != null && m instanceof AccessibleObject;
	}

	public void setAccessible(boolean b)
	{
		if (this.isAccessibleObject)
			((AccessibleObject) this.theMember).setAccessible(b);
	}

	public boolean addModifier(int modifier)
	{
		if (this.theMember == null)
			return false;

		Field modifiersField = this.getModifiersField();

		if (modifiersField == null)
			return false;

		boolean accessible = this.isAccessibleObject
				&& ((AccessibleObject) this.theMember).isAccessible();

		try
		{
			modifiersField.setAccessible(true);
			if (this.isAccessibleObject && !accessible)
				((AccessibleObject) this.theMember).setAccessible(true);
			modifiersField.set(this.theMember, this.theMember.getModifiers()
					| modifier);
			if (this.isAccessibleObject && !accessible)
				((AccessibleObject) this.theMember).setAccessible(false);
			// should I just keep these accessible? can't decide
			modifiersField.setAccessible(false);
			return true;
		} catch (Exception e)
		{
			// should I just keep these accessible? can't decide
			if (modifiersField != null)
				modifiersField.setAccessible(false);
			if (this.isAccessibleObject
					&& accessible != ((AccessibleObject) this.theMember)
							.isAccessible())
				((AccessibleObject) this.theMember).setAccessible(accessible);
			return false;
		}
	}

	public boolean removeModifier(int modifier)
	{
		if (this.theMember == null)
			return false;

		Field modifiersField = this.getModifiersField();

		if (modifiersField == null)
			return false;

		boolean accessible = this.isAccessibleObject
				&& ((AccessibleObject) this.theMember).isAccessible();

		try
		{
			modifiersField.setAccessible(true);
			if (this.isAccessibleObject && !accessible)
				((AccessibleObject) this.theMember).setAccessible(true);
			modifiersField.set(this.theMember, this.theMember.getModifiers()
					& ~modifier);
			if (this.isAccessibleObject && !accessible)
				((AccessibleObject) this.theMember).setAccessible(false);

			// should I just keep these accessible? can't decide
			modifiersField.setAccessible(false);
			return true;
		} catch (Exception e)
		{
			// should I just keep these accessible? can't decide
			if (modifiersField != null)
				modifiersField.setAccessible(false);
			if (this.isAccessibleObject
					&& accessible != ((AccessibleObject) this.theMember)
							.isAccessible())
				((AccessibleObject) this.theMember).setAccessible(accessible);
			return false;
		}
	}

	/**
	 * Clears the modifiers and replaces them with the ones supplied.
	 * 
	 * @param modifiers
	 *            The new modifiers.
	 * @return Whether the operation was successful.
	 */
	public boolean setModifiers(int... modifiers)
	{
		if(this.theMember == null)
			return false;
		Field modifiersField = this.getModifiersField();
		if(modifiersField == null)
			return false;
		boolean modifiersGiven = modifiers != null && modifiers.length != 0;
		
		try
		{
			// Clear the modifiers.
			modifiersField.setAccessible(true);
			modifiersField.set(this.theMember, 0);
			// Replace them with ours.
			if(modifiersGiven)
			{
				int ourMods = 0;
				for(int i : modifiers)
					ourMods |= i;
				modifiersField.set(this.theMember, ourMods);
			}
			modifiersField.setAccessible(false);
			return true;
		} catch (Exception e)
		{
			modifiersField.setAccessible(false);
			
			return false;
		}
	}

	protected Field getModifiersField()
	{
		return this.theMember instanceof Method ? methodModifiersField
				: this.theMember instanceof Field ? fieldModifiersField
						: this.theMember instanceof Constructor ? constructorModifiersField
								: null;
	}

	public static FieldWrapper getFieldWrapperFromClass(Class<?> c,
			String fieldName) throws NoSuchFieldException, SecurityException
	{
		return new FieldWrapper(c.getDeclaredField(fieldName));
	}

	public static MethodWrapper getMethodWrapperFromClass(Class<?> c,
			String methodName, Class<?>... paramVarArgs)
			throws NoSuchMethodException, SecurityException
	{
		return new MethodWrapper(c.getDeclaredMethod(methodName, paramVarArgs));
	}

	public static ConstructorWrapper<?> getConstructorWrapperFromClass(
			Class<?> c, Class<?>... classes) throws NoSuchMethodException,
			SecurityException
	{
		/*
		 * Ugh, I wish there were a better way of doing this. Looks like we're
		 * stuck with Java 1.7. If we take away the diamond, it would be less
		 * nice and functional, but it would work with more of the previous java
		 * versions.
		 * 
		 * Whatever, if people want to use this, they can either upgrade the
		 * hell out of the stone ages or modify this part themselves via the
		 * source or bytecode modification or whatever their method of choice
		 * might be.
		 */

		return new ConstructorWrapper<>(c.getConstructor(classes));
	}

	public boolean hasModifier(int modifier)
	{
		return (this.theMember.getModifiers() & modifier) != 0;
	}
}
