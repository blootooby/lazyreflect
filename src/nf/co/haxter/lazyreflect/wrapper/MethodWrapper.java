package nf.co.haxter.lazyreflect.wrapper;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MethodWrapper extends MemberWrapper
{
	public MethodWrapper(Method m)
	{
		super(m);
	}

	public Method getMethod()
	{
		return (Method) this.theMember;
	}

	public boolean invokeMethod(Object paramObject, Object... args)
	{
		try
		{
			this.getMethod().invoke(paramObject, args);
			return true;
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e)
		{
			return false;
		}
	}
}
